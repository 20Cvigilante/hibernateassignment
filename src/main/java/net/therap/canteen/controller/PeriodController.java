package net.therap.canteen.controller;

import net.therap.canteen.dao.PeriodDao;
import net.therap.canteen.entity.Period;
import net.therap.canteen.entity.Meal;

import java.util.*;

import static net.therap.canteen.util.CanteenConstants.MAX_DAYS;
import static net.therap.canteen.util.CanteenUtility.*;

/**
 * @author musa.khan
 * @since 27/11/2020
 */
public class PeriodController {

    private static final PeriodDao periodDao = new PeriodDao();

    public static void printMealsForPeriod(Scanner scanner) {
        String periodPrompt = "Enter number corresponding target meal-time:";
        int periodId = getPeriodIdToUpdate(scanner, periodPrompt);
        printItems(periodDao.getMealsForPeriodId(periodId));
    }

    public static void insertNewPeriod(Scanner scanner) {
        String periodPrompt = "Enter new period name";
        String newPeriodName = awaitStringInput(scanner, periodPrompt);
        Period period = periodDao.getPeriodByName(newPeriodName);

        if (Objects.nonNull(period)) {
            System.out.println("Period with that name already exists!");

            return;
        }

        String totalDaysInputPrompt = "On how many days do you want the new period?";
        int dayCount = awaitIntegerInput(scanner, totalDaysInputPrompt, MAX_DAYS);
        printAllDays();

        List<Integer> daysSelected = new ArrayList<>();

        for (int i = 0; i < dayCount; i++) {

            String dayInputPrompt = "Enter number corresponding to day above, for the new period:";
            daysSelected.add(awaitIntegerInput(scanner, dayInputPrompt, MAX_DAYS) - 1);
        }

        boolean status = periodDao.insertPeriod(newPeriodName, daysSelected);

        logOperationResult(status);
    }

    public static void addDayForPeriod(Scanner scanner) {
        String periodPrompt = "Enter number corresponding target meal-time:";
        String dayPrompt = "Enter number corresponding day in which this meal-time will be available:";

        int periodId = getPeriodIdToUpdate(scanner, periodPrompt);
        int dayId = getDayIdToUpdate(scanner, dayPrompt);
        boolean status = periodDao.addDayForPeriod(periodId, dayId);

        logOperationResult(status);
    }

    public static void updatePeriodName(Scanner scanner) {
        String currentNamePrompt = "Enter number corresponding item of old name:";
        String newNamePrompt = "Enter the new name for the specified item:";

        int periodId = getPeriodIdToUpdate(scanner, currentNamePrompt);
        String newName = getNewNameToInsert(scanner, newNamePrompt);
        boolean status = periodDao.updatePeriodName(periodId, newName);

        logOperationResult(status);
    }

    public static void removePeriodForDay(Scanner scanner) {
        String periodPrompt = "Enter number corresponding target meal-time:";
        String dayPrompt = "Enter number corresponding day from which this meal-time will be removed:";

        int periodId = getPeriodIdToUpdate(scanner, periodPrompt);
        int dayId = getDayIdToUpdate(scanner, dayPrompt);
        boolean status = periodDao.removeDayForPeriod(periodId, dayId);

        logOperationResult(status);
    }

    public static void cancelPeriod(Scanner scanner) {
        String periodPrompt = "Enter number corresponding target meal-time:";

        int periodId = getPeriodIdToUpdate(scanner, periodPrompt);

        boolean status = periodDao.removePeriod(periodId);

        logOperationResult(status);
    }

    public static void replaceMealForPeriod(Scanner scanner) {
        String periodPrompt = "Enter number corresponding target meal-time:";
        String currentMealPrompt = "These are currently served meals at this meal-time. Enter number corresponding " +
                "current meal to remove it:";
        String newMealPrompt = "Enter number corresponding new meal of choice:";

        int targetPeriodId = getPeriodIdToUpdate(scanner, periodPrompt);
        Set<Meal> mealsForTargetPeriod = periodDao.getMealsForPeriodId(targetPeriodId);

        printItems(mealsForTargetPeriod);
        Map<Integer, Integer> integerToIdMap = getOrderedNumbersMappedToDBId(mealsForTargetPeriod);

        int currentMealId = integerToIdMap.get(awaitIntegerInput(scanner, currentMealPrompt, integerToIdMap.size()));
        int newMealId = getMealIdToUpdate(scanner, newMealPrompt);

        boolean status = periodDao.replaceMealForPeriod(targetPeriodId, currentMealId, newMealId);

        logOperationResult(status);
    }
}