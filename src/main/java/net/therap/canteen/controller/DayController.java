package net.therap.canteen.controller;

import net.therap.canteen.dao.DayDao;

import java.util.Scanner;

import static net.therap.canteen.util.CanteenUtility.*;

/**
 * @author musa.khan
 * @since 27/11/2020
 */
public class DayController {

    private static final DayDao dayDao = new DayDao();

    public static void printMealsForDay(Scanner scanner) {
        String dayPrompt = "Enter integer corresponding day for which meals will be printed:";
        int requestedDayId = getDayIdToUpdate(scanner, dayPrompt);
        printItems(dayDao.getMealsForDayId(requestedDayId));
    }

    public static void printPeriodsForDay(Scanner scanner) {
        String dayPrompt = "Enter integer corresponding day for which meal-times will be printed:";
        int requestedDayId = getDayIdToUpdate(scanner, dayPrompt);
        printItems(dayDao.getPeriodsForDayId(requestedDayId));
    }
}