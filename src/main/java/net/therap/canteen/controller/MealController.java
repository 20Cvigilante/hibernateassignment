package net.therap.canteen.controller;

import net.therap.canteen.dao.PeriodDao;
import net.therap.canteen.dao.MealDao;
import net.therap.canteen.entity.Day;
import net.therap.canteen.entity.Period;
import net.therap.canteen.entity.Meal;

import java.util.*;

import static net.therap.canteen.util.CanteenConstants.MAX_DAYS;
import static net.therap.canteen.util.CanteenUtility.*;

/**
 * @author musa.khan
 * @since 27/11/2020
 */
public class MealController {

    private static final MealDao mealDao = new MealDao();

    public static void insertNewMeal(Scanner scanner) {
        String newNamePrompt = "Enter name of new meal (valid inputs only!):";
        String newMealName = getNewNameToInsert(scanner, newNamePrompt);
        Meal meal = mealDao.getMealByName(newMealName);

        if (Objects.nonNull(meal)) {
            System.out.println("Meal with that name already exists!");

            return;
        }

        System.out.println("These are the hours currently available.");
        Set<Period> periods = PeriodDao.getAllPeriods();
        printItems(periods);

        Map<Integer, Integer> hoursIdMap = getOrderedNumbersMappedToDBId(periods);
        String inputPromptHour = "For how many of the above hours do you want to serve the new meal?";
        int hourCount = awaitIntegerInput(scanner, inputPromptHour, hoursIdMap.size());

        List<Integer> hoursSelected = new ArrayList<>();

        for (int i = 0; i < hourCount; i++) {

            String inputPrompt = "Enter number corresponding the meal-time above, to serve the new " + "meal:";
            int hourNumber = awaitIntegerInput(scanner, inputPrompt, hoursIdMap.size());
            hoursSelected.add(hoursIdMap.get(hourNumber));
        }

        String inputPromptDay = "On how many days do you want the new meal?";
        int dayCount = awaitIntegerInput(scanner, inputPromptDay, MAX_DAYS);
        printAllDays();

        List<Integer> daysSelected = new ArrayList<>();

        for (int i = 0; i < dayCount; i++) {

            String inputPrompt = "Enter number corresponding to day above, for serving the new meal:";
            daysSelected.add(awaitIntegerInput(scanner, inputPrompt, MAX_DAYS) - 1);
        }

        boolean status = mealDao.insertMeal(newMealName, hoursSelected, daysSelected);

        logOperationResult(status);
    }

    public static void addMealServingDay(Scanner scanner) {
        String mealPrompt = "Enter number corresponding to target meal:";
        String dayPrompt = "Enter number corresponding to day to serve this meal:";

        int mealId = getMealIdToUpdate(scanner, mealPrompt);
        int dayId = getDayIdToUpdate(scanner, dayPrompt);

        boolean status = mealDao.addDayForMeal(mealId, dayId);

        logOperationResult(status);
    }

    public static void addMealServingPeriod(Scanner scanner) {
        String mealPrompt = "Enter number corresponding to target meal:";
        String hourPrompt = "Enter number corresponding to target meal-time:";

        int mealId = getMealIdToUpdate(scanner, mealPrompt);
        int hourId = getPeriodIdToUpdate(scanner, hourPrompt);

        boolean status = mealDao.addPeriodForMeal(mealId, hourId);

        logOperationResult(status);
    }

    public static void updateMealName(Scanner scanner) {
        String currentNamePrompt = "Enter number corresponding item of current name:";
        String newNamePrompt = "Enter the new name for the specified item:";

        int mealId = getMealIdToUpdate(scanner, currentNamePrompt);
        String newName = getNewNameToInsert(scanner, newNamePrompt);

        boolean status = mealDao.updateMealName(mealId, newName);

        logOperationResult(status);
    }

    public static void replaceDayForMeal(Scanner scanner) {
        String mealPrompt = "Enter number corresponding target meal:";
        String currentDayPrompt = "These are the current days. Enter number corresponding day from which you want to " +
                "remove this meal:";
        String newDayPrompt = "Enter number corresponding new day to serve this meal:";

        int mealId = getMealIdToUpdate(scanner, mealPrompt);
        Set<Day> daysForTargetMeal = mealDao.getDaysForMealId(mealId);

        printItems(daysForTargetMeal);
        Map<Integer, Integer> integerToIdMap = getOrderedNumbersMappedToDBId(daysForTargetMeal);

        int currentDayId = integerToIdMap.get(awaitIntegerInput(scanner, currentDayPrompt, integerToIdMap.size()));
        int newDayId = getDayIdToUpdate(scanner, newDayPrompt);

        boolean status = mealDao.replaceDayForMeal(mealId, currentDayId, newDayId);

        logOperationResult(status);
    }

    public static void replacePeriodForMeal(Scanner scanner) {
        String mealPrompt = "Enter number corresponding target meal:";
        String currentHourPrompt = "These are the current meal-times. Enter number corresponding to a meal-time from " +
                "which you want to remove this meal:";
        String newHourPrompt = "Enter number corresponding new meal-time:";

        int mealId = getMealIdToUpdate(scanner, mealPrompt);
        Set<Period> hoursForTargetMeal = mealDao.getPeriodsForMealId(mealId);

        printItems(hoursForTargetMeal);
        Map<Integer, Integer> integerToIdMap = getOrderedNumbersMappedToDBId(hoursForTargetMeal);

        int currentHourId = integerToIdMap.get(awaitIntegerInput(scanner, currentHourPrompt, integerToIdMap.size()));
        int newHourId = getPeriodIdToUpdate(scanner, newHourPrompt);

        boolean status = mealDao.replacePeriodForMeal(mealId, currentHourId, newHourId);

        logOperationResult(status);
    }

    public static void cancelMealServingDay(Scanner scanner) {
        String mealPrompt = "Enter number corresponding target meal:";
        String dayPrompt = "Enter number corresponding day to delete this meal from:";

        int mealId = getMealIdToUpdate(scanner, mealPrompt);
        int dayId = getDayIdToUpdate(scanner, dayPrompt);

        boolean status = mealDao.removeDayForMeal(mealId, dayId);

        logOperationResult(status);
    }

    public static void cancelMealServingPeriod(Scanner scanner) {
        String mealPrompt = "Enter number corresponding target meal:";
        String hourPrompt = "Enter number corresponding target meal-time:";

        int mealId = getMealIdToUpdate(scanner, mealPrompt);
        int hourId = getPeriodIdToUpdate(scanner, hourPrompt);

        boolean status = mealDao.removePeriodForMeal(mealId, hourId);

        logOperationResult(status);
    }

    public static void cancelMeal(Scanner scanner) {
        String mealPrompt = "Enter number corresponding target meal:";
        boolean status = mealDao.removeMeal(getMealIdToUpdate(scanner, mealPrompt));

        logOperationResult(status);
    }
}