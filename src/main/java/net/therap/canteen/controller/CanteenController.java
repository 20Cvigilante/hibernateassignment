package net.therap.canteen.controller;

import net.therap.canteen.dao.*;

import java.util.Arrays;
import java.util.Scanner;

import static net.therap.canteen.util.CanteenConstants.*;
import static net.therap.canteen.util.CanteenUtility.*;
import static net.therap.canteen.util.CanteenView.*;

/**
 * @author musa.khan
 * @since 27/11/2020
 */
public class CanteenController {

    private static final int MAX_OPTION = 22;

    public static void main(String[] args) {
        viewWelcome();
        presentOptions(Arrays.asList(CANTEEN_OPERATIONS));
        interactWithUser();
    }

    public static void interactWithUser() {
        Scanner scanner = new Scanner(System.in);
        int userInput = awaitIntegerInput(scanner, OPTION_PROMPT, MAX_OPTION);

        while (true) {

            switch (userInput) {

                case 0:
                    break;

                case 1:
                    viewWelcome();
                    break;

                case 2:
                    viewMenu();
                    break;

                case 3:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[3]);
                    printItems(MealDao.getAllMeals());
                    break;

                case 4:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[4]);
                    printItems(PeriodDao.getAllPeriods());
                    break;

                case 5:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[5]);
                    PeriodController.printMealsForPeriod(scanner);
                    break;

                case 6:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[6]);
                    DayController.printMealsForDay(scanner);
                    break;

                case 7:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[7]);
                    DayController.printPeriodsForDay(scanner);
                    break;

                case 8:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[8]);
                    MealController.insertNewMeal(scanner);
                    break;

                case 9:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[9]);
                    MealController.addMealServingDay(scanner);
                    break;

                case 10:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[10]);
                    MealController.addMealServingPeriod(scanner);
                    break;

                case 11:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[11]);
                    MealController.updateMealName(scanner);
                    break;

                case 12:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[12]);
                    MealController.replaceDayForMeal(scanner);
                    break;

                case 13:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[13]);
                    MealController.replacePeriodForMeal(scanner);
                    break;

                case 14:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[14]);
                    MealController.cancelMealServingDay(scanner);
                    break;

                case 15:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[15]);
                    MealController.cancelMealServingPeriod(scanner);
                    break;

                case 16:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[16]);
                    MealController.cancelMeal(scanner);
                    break;

                case 17:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[17]);
                    PeriodController.insertNewPeriod(scanner);
                    break;

                case 18:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[18]);
                    PeriodController.addDayForPeriod(scanner);
                    break;

                case 19:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[19]);
                    PeriodController.updatePeriodName(scanner);
                    break;

                case 20:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[20]);
                    PeriodController.replaceMealForPeriod(scanner);
                    break;

                case 21:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[21]);
                    PeriodController.removePeriodForDay(scanner);
                    break;

                case 22:
                    System.out.println("Selected option: " + CANTEEN_OPERATIONS[22]);
                    PeriodController.cancelPeriod(scanner);
                    break;

                default:
                    System.out.println("Enter valid numbers only!");
                    break;
            }

            if (userInput == 0) {
                System.out.println("Exiting!");
                break;
            }
            else {
                presentOptions(userInput == 1 ? Arrays.asList(CANTEEN_OPERATIONS) : Arrays.asList(NAVIGATION));
                userInput = awaitIntegerInput(scanner, OPTION_PROMPT, MAX_OPTION);
            }
        }

        scanner.close();
    }
}