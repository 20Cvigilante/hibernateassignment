package net.therap.canteen.util;

import net.therap.canteen.dao.PeriodDao;
import net.therap.canteen.dao.MealDao;
import net.therap.canteen.entity.CanteenEntity;
import net.therap.canteen.entity.Period;
import net.therap.canteen.entity.Meal;

import java.util.*;

import static net.therap.canteen.util.CanteenConstants.MAX_DAYS;
import static net.therap.canteen.util.CanteenConstants.MAX_INPUT_LENGTH;

/**
 * @author musa.khan
 * @since 06/12/2020
 */
public class CanteenUtility {

    public static void presentOptions(List<String> options) {
        System.out.println("\nOptions: ");

        for (int i = 0; i < options.size(); i++) {
            System.out.printf("%d) %s \n", i, options.get(i));
        }

        System.out.println();
    }

    public static String getNewNameToInsert(Scanner scanner, String newNamePrompt) {
        return awaitStringInput(scanner, newNamePrompt);
    }

    public static int getMealIdToUpdate(Scanner scanner, String mealPrompt) {
        Set<Meal> allMeals = MealDao.getAllMeals();
        printItems(allMeals);

        Map<Integer, Integer> mealsIdMap = getOrderedNumbersMappedToDBId(allMeals);

        int userInput = awaitIntegerInput(scanner, mealPrompt, mealsIdMap.size());

        return mealsIdMap.get(userInput);
    }

    public static int getPeriodIdToUpdate(Scanner scanner, String periodPrompt) {
        Set<Period> setOfPeriods = PeriodDao.getAllPeriods();
        printItems(setOfPeriods);

        Map<Integer, Integer> periodsIdMap = getOrderedNumbersMappedToDBId(setOfPeriods);
        int userInput = awaitIntegerInput(scanner, periodPrompt, periodsIdMap.size());

        return periodsIdMap.get(userInput);
    }

    public static int getDayIdToUpdate(Scanner scanner, String dayPrompt) {
        printAllDays();
        return awaitIntegerInput(scanner, dayPrompt, MAX_DAYS);
    }

    public static int awaitIntegerInput(Scanner scanner, String inputPrompt, int max) {
        boolean validInput = false;
        int input = 0;
        int min = 0;

        while (!validInput) {

            try {

                System.out.println(inputPrompt);
                input = Integer.parseInt(scanner.nextLine());
                validInput = input >= min && input <= max;

            } catch (InputMismatchException | NumberFormatException e) {

                validInput = false;
                System.out.println("Please enter integers only!");

            }
        }

        return input;
    }

    public static String awaitStringInput(Scanner scanner, String inputPrompt) {
        boolean validInput = false;
        String input = null;

        while (!validInput) {

            try {

                System.out.println(inputPrompt);
                input = scanner.nextLine();
                validInput = input.length() > 0 && input.length() < MAX_INPUT_LENGTH;

            } catch (InputMismatchException e) {

                validInput = false;
                System.out.println("Please enter Strings only!");

            }

        }

        return input;
    }

    public static void printItems(Set<? extends CanteenEntity> items) {
        Object[] itemArray = items.toArray();

        for (int i = 0; i < items.size(); ++i) {
            System.out.println(i + 1 + ") " + ((CanteenEntity) itemArray[i]).getName());
        }
    }

    public static void printAllDays() {
        for (int i = 0; i < MAX_DAYS; ++i) {
            System.out.println(i + 1 + ") " + getDayFromEnum(i));
        }
    }

    public static Map<Integer, Integer> getOrderedNumbersMappedToDBId(Set<? extends CanteenEntity> items) {
        Object[] itemArray = items.toArray();
        Map<Integer, Integer> integerToIdMap = new HashMap<>();

        for (int i = 0; i < items.size(); ++i) {
            integerToIdMap.put(i + 1, ((CanteenEntity) itemArray[i]).getId());
        }

        return integerToIdMap;
    }

    public static void logOperationResult(boolean status) {
        if (status) {
            System.out.println("Operation successful!");
        } else {
            System.out.println("Operation not processed!");
        }
    }

    public static String getDayFromEnum(int position) {
        String dayNameUpperCase = Days.values()[position].toString();
        StringBuilder dayNameLowerCase = new StringBuilder();
        dayNameLowerCase.append(dayNameUpperCase.charAt(0));

        for (int i = 1; i < dayNameUpperCase.length(); ++i) {
            dayNameLowerCase.append((char) (dayNameUpperCase.charAt(i) + 32));
        }

        return dayNameLowerCase.toString();
    }
}