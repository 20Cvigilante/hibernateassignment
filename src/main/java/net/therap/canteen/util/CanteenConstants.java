package net.therap.canteen.util;

/**
 * @author musa.khan
 * @since 06/12/2020
 */
public class CanteenConstants {

    public static final int MAX_DAYS = 5;
    public static final int MAX_INPUT_LENGTH = 100;
    public static final String OPTION_PROMPT = "Enter integer corresponding to option:";

    public static final String[] CANTEEN_OPERATIONS = {"Exit", "Welcome Screen", "View Menu", "View All Meals",
            "View All Meal Periods", "View Meals for Period", "View Meals for Day", "View Periods for Day",
            "Insert New Meal", "Add Meal's Serving Day", "Add Meal's Serving Period", "Update Meal Name",
            "Replace Meal's Serving Day", "Replace Meal's Serving Period", "Cancel Meal for Day",
            "Cancel Meal for Period", "Cancel Meal", "Insert New Meal-Period", "Add Day for Meal-Period", "Update"
            + " Meal-Period Name", "Replace Meal for an Period", "Cancel Meal-Period for Day", "Cancel Meal-Period"};
    public static final String[] NAVIGATION = {"Exit", "Canteen Home"};
}