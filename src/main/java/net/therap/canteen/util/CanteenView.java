package net.therap.canteen.util;

import net.therap.canteen.dao.DayDao;
import net.therap.canteen.dao.PeriodDao;
import net.therap.canteen.entity.Meal;
import net.therap.canteen.entity.Period;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static net.therap.canteen.util.CanteenUtility.getDayFromEnum;

/**
 * @author musa.khan
 * @since 27/11/2020
 */
public class CanteenView {

    private static final int GAP_MED = 20;
    private static final String COL_DAY = "#DAY#";
    private static final String COL_MEAL_TIME = "#MEAL TIME#";
    private static final String COL_MEALS = "#MEALS#";
    private static final String WELCOME_MSG = "\nWELCOME TO THERAP CANTEEN HOME!";

    public static void viewWelcome() {
        System.out.println(WELCOME_MSG);
    }

    public static void viewMenu() {
        System.out.println("******");
        printTableData(Arrays.asList(COL_DAY, COL_MEAL_TIME, COL_MEALS));
        getTableData();
        System.out.println("******");
    }

    private static void getTableData() {
        DayDao dayDao = new DayDao();
        PeriodDao periodDao = new PeriodDao();

        System.out.println("-");

        for (int i = 1; i <= 5; ++i) {
            String currentDay = getDayFromEnum(i - 1);
            Set<Period> periodsAssociatedWithDay = dayDao.getPeriodsForDayId(i);

            if (periodsAssociatedWithDay.size() == 0) {

                continue;
            }

            Set<Meal> mealsAssociatedWithDay = dayDao.getMealsForDayId(i);

            if (mealsAssociatedWithDay.size() == 0) {

                continue;
            }

            for (Period period : periodsAssociatedWithDay) {

                Set<Meal> mealsAssociatedWithHour = periodDao.getMealsForPeriodId(period.getId());
                StringBuilder mealsForThisDayAndPeriod = new StringBuilder();

                for (Meal meal : mealsAssociatedWithHour) {

                    if (mealsAssociatedWithDay.contains(meal)) {
                        mealsForThisDayAndPeriod.append(meal.getName()).append(", ");
                    }
                }

                if (mealsForThisDayAndPeriod.length() > 2) {
                    mealsForThisDayAndPeriod = new StringBuilder(mealsForThisDayAndPeriod.substring(0,
                            mealsForThisDayAndPeriod.length() - 2));
                }

                printTableData(Arrays.asList(currentDay, period.getName(), mealsForThisDayAndPeriod.toString()));
            }

            System.out.println("-");
        }
    }

    public static void printTableData(List<String> row) {
        for (String column : row) {

            System.out.print(column);
            printGaps(GAP_MED - column.length());
        }

        System.out.println();
    }

    private static void printGaps(int gaps) {
        for (int i = 0; i < gaps; i++) {
            System.out.print(" ");
        }
    }
}