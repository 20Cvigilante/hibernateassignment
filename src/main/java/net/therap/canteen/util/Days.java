package net.therap.canteen.util;

/**
 * @author musa.khan
 * @since 07/12/2020
 */

public enum Days {

    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY
}