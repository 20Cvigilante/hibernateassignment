package net.therap.canteen.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author musa.khan
 * @since 27/11/2020
 */
@Entity
@Table(name = "periods")
public class Period implements CanteenEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToMany(cascade = {CascadeType.PERSIST,
            CascadeType.MERGE,
            CascadeType.DETACH,
            CascadeType.REFRESH},
            fetch = FetchType.LAZY)
    @JoinTable(name = "days_periods",
            joinColumns = {@JoinColumn(name = "period_id")},
            inverseJoinColumns = {@JoinColumn(name = "day_id")})
    private Set<Day> days;

    @ManyToMany(cascade = {CascadeType.PERSIST,
            CascadeType.MERGE,
            CascadeType.DETACH,
            CascadeType.REFRESH},
            fetch = FetchType.LAZY)
    @JoinTable(name = "periods_meals",
            joinColumns = {@JoinColumn(name = "period_id")},
            inverseJoinColumns = {@JoinColumn(name = "meal_id")})
    private Set<Meal> meals;

    public Period() {

    }

    public Period(String name) {
        this.name = name;
        this.days = new HashSet<>();
        this.meals = new HashSet<>();
    }

    @Override
    public int getId() {
        
        return id;
    }

    @Override
    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Day> getDays() {

        return this.days;
    }

    public void setDays(Set<Day> days) {
        this.days = days;
    }

    public Set<Meal> getMeals() {

        return this.meals;
    }

    public void setMeals(Set<Meal> meals) {
        this.meals = meals;
    }

    public boolean addDay(Day day) {
        if (Objects.isNull(days)) {
            days = new HashSet<>();
        }

        return days.add(day);
    }

    public boolean removeDay(Day day) {
        if (Objects.isNull(days)) {

            return false;
        }

        return days.remove(day);
    }

    public boolean addMeal(Meal meal) {
        if (Objects.isNull(meals)) {
            meals = new HashSet<>();
        }

        return meals.add(meal);
    }

    public boolean removeMeal(Meal meal) {
        if (Objects.isNull(meals)) {

            return false;
        }

        return meals.remove(meal);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {

            return true;
        }

        if (o == null || getClass() != o.getClass()) {

            return false;
        }

        Period that = (Period) o;

        return id == that.id && name.equals(that.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Period{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", days=" + days +
                ", meals=" + meals + '}';
    }
}