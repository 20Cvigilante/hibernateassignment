package net.therap.canteen.entity;

import javax.persistence.*;
import java.util.*;

/**
 * @author musa.khan
 * @since 27/11/2020
 */
@Entity
@Table(name = "days")
public class Day implements CanteenEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToMany(cascade = {CascadeType.PERSIST,
            CascadeType.MERGE,
            CascadeType.DETACH,
            CascadeType.REFRESH},
            fetch = FetchType.LAZY)
    @JoinTable(name = "days_periods",
            joinColumns = {@JoinColumn(name = "day_id")},
            inverseJoinColumns = {@JoinColumn(name = "period_id")})
    private Set<Period> periods;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE,
            CascadeType.DETACH,
            CascadeType.REFRESH},
            fetch = FetchType.LAZY)
    @JoinTable(name = "days_meals",
            joinColumns = {@JoinColumn(name = "day_id")},
            inverseJoinColumns = {@JoinColumn(name = "meal_id")})
    private Set<Meal> meals;

    public Day() {

    }

    public Day(String name) {
        this.name = name;
        this.periods = new HashSet<>();
        this.meals = new HashSet<>();
    }

    @Override
    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Period> getHours() {

        return periods;
    }

    public void setPeriods(Set<Period> periods) {
        this.periods = periods;
    }

    public Set<Meal> getMeals() {

        return meals;
    }

    public void setMeals(Set<Meal> meals) {
        this.meals = meals;
    }

    public boolean addPeriod(Period period) {
        if (Objects.isNull(periods)) {
            periods = new HashSet<>();
        }

        return periods.add(period);
    }

    public boolean removePeriod(Period period) {
        if (Objects.isNull(periods)) {

            return false;
        }

        return periods.remove(period);
    }

    public boolean addMeal(Meal meal) {
        if (Objects.isNull(meals)) {

            meals = new HashSet<>();
        }

        return meals.add(meal);
    }

    public boolean removeMeal(Meal meal) {
        if (Objects.isNull(meals)) {

            return false;
        }

        return meals.remove(meal);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {

            return true;
        }

        if (o == null || getClass() != o.getClass()) {

            return false;
        }

        Day day = (Day) o;

        return id == day.id && name.equals(day.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Day{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", periods=" + periods +
                ", meals=" + meals + '}';
    }
}