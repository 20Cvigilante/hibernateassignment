package net.therap.canteen.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * @author musa.khan
 * @since 27/11/2020
 */
@Entity
@Table(name = "meals")
public class Meal implements CanteenEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToMany(cascade = {CascadeType.PERSIST,
            CascadeType.MERGE,
            CascadeType.DETACH,
            CascadeType.REFRESH},
            fetch = FetchType.LAZY)
    @JoinTable(name = "days_meals",
            joinColumns = {@JoinColumn(name = "meal_id")},
            inverseJoinColumns = {@JoinColumn(name = "day_id")})
    private Set<Day> days;

    @ManyToMany(cascade = {CascadeType.PERSIST,
            CascadeType.MERGE,
            CascadeType.DETACH,
            CascadeType.REFRESH},
            fetch = FetchType.LAZY)
    @JoinTable(name = "periods_meals",
            joinColumns = {@JoinColumn(name = "meal_id")},
            inverseJoinColumns = {@JoinColumn(name = "period_id")})
    private Set<Period> periods;

    public Meal() {

    }

    public Meal(String name) {
        this.name = name;
        this.periods = new HashSet<>();
        this.days = new HashSet<>();
    }

    @Override
    public int getId() {

        return id;
    }

    @Override
    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Period> getPeriods() {

        return this.periods;
    }

    public void setPeriods(Set<Period> periods) {
        this.periods = periods;
    }

    public Set<Day> getDays() {

        return this.days;
    }

    public void setDays(Set<Day> days) {
        this.days = days;
    }

    public boolean addDay(Day day) {
        if (Objects.isNull(days)) {
            days = new HashSet<>();
        }

        return days.add(day);
    }

    public boolean removeDay(Day day) {
        if (Objects.isNull(days)) {

            return false;
        }

        return days.remove(day);
    }

    public boolean addPeriod(Period period) {
        if (Objects.isNull(periods)) {
            periods = new HashSet<>();
        }

        return periods.add(period);
    }

    public boolean removePeriod(Period period) {
        if (Objects.isNull(periods)) {

            return false;
        }

        return periods.remove(period);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {

            return true;
        }

        if (o == null || getClass() != o.getClass()) {

            return false;
        }

        Meal meal = (Meal) o;

        return id == meal.id && name.equals(meal.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Meal{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", days=" + days +
                ", periods=" + periods + '}';
    }
}