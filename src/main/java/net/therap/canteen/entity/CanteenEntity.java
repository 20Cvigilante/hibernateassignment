package net.therap.canteen.entity;

/**
 * @author musa.khan
 * @since 27/11/2020
 */
public interface CanteenEntity {

    int getId();

    String getName();
}