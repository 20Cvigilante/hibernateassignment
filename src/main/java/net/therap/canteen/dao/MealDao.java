package net.therap.canteen.dao;

import net.therap.canteen.entity.Day;
import net.therap.canteen.entity.Period;
import net.therap.canteen.entity.Meal;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static net.therap.canteen.dao.CanteenDao.getEntityManager;
import static net.therap.canteen.util.CanteenUtility.getDayFromEnum;

/**
 * @author musa.khan
 * @since 05/12/2020
 */
public class MealDao {

    private static final String GET_DAY_BY_NAME_JPQL = "SELECT d " +
                                                        "FROM Day d " +
                                                        "WHERE d.name = :name";
    private static final String GET_PERIOD_BY_ID_JPQL = "SELECT p " +
                                                        "FROM Period p " +
                                                        "WHERE p.id = :id";
    private static final String GET_MEAL_BY_NAME_JPQL = "SELECT m " +
                                                        "FROM Meal m " +
                                                        "WHERE m.name = :name";
    private static final String ALL_MEALS_QUERY_JPQL = "SELECT meals " +
                                                        "FROM Meal meals";
    private static final String DAYS_FOR_MEALID_JPQL = "SELECT m " +
                                                        "FROM Meal m " +
                                                        "LEFT JOIN FETCH m.days " +
                                                        "WHERE m.id = :id";
    private static final String PERIODS_FOR_MEALID_JPQL = "SELECT m " +
                                                        "FROM Meal m " +
                                                        "LEFT JOIN FETCH m.periods " +
                                                        "WHERE m.id = :id";

    public boolean insertMeal(String mealName, List<Integer> periodsSelected, List<Integer> daysSelected) {
        boolean status = false;

        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            Meal meal = new Meal(mealName);

            for (Integer periodSelected : periodsSelected) {

                TypedQuery<Period> queryHour = entityManager.createQuery(GET_PERIOD_BY_ID_JPQL, Period.class);
                queryHour.setParameter("id", periodSelected);
                Period targetPeriod = queryHour.getSingleResult();

                meal.addPeriod(targetPeriod);
            }

            for (Integer daySelected : daysSelected) {

                String dayName = getDayFromEnum(daySelected);
                TypedQuery<Day> queryDay = entityManager.createQuery(GET_DAY_BY_NAME_JPQL, Day.class);
                queryDay.setParameter("name", dayName);
                Day targetDay = queryDay.getSingleResult();

                meal.addDay(targetDay);
            }

            entityManager.persist(meal);

            entityManager.getTransaction().commit();
            entityManager.close();
            status = true;

        } catch (PersistenceException e) {
            entityManager.close();

            if (e.getMessage().contains("duplicate")) {
                System.out.println("Could not be added. Does it already exist?");
            }
            else {
                System.out.println("There was an error. Operation failed!");
                e.printStackTrace();
            }
        }

        return status;
    }

    public Meal getMealByName(String mealName) {
        Meal meal = null;
        EntityManager entityManager = getEntityManager();

        try {
            TypedQuery<Meal> query = entityManager.createQuery(GET_MEAL_BY_NAME_JPQL, Meal.class);
            query.setParameter("name", mealName);
            meal = query.getSingleResult();
            entityManager.close();

        } catch (PersistenceException | IllegalArgumentException e) {
            entityManager.close();
        }

        return meal;
    }

    public Meal getMealById(int mealId) {
        Meal meal = null;
        EntityManager entityManager = getEntityManager();

        try {
            meal = entityManager.find(Meal.class, mealId);
            entityManager.close();

        } catch (PersistenceException e) {
            entityManager.close();
        }

        return meal;
    }

    public static Set<Meal> getAllMeals() {
        EntityManager entityManager = getEntityManager();
        Set<Meal> mealsSet = null;

        try {
            TypedQuery<Meal> allMealsQuery = entityManager.createQuery(ALL_MEALS_QUERY_JPQL, Meal.class);
            List<Meal> allMeals = allMealsQuery.getResultList();

            mealsSet = new HashSet<>(allMeals);

            entityManager.close();

        } catch (PersistenceException e) {
            entityManager.close();
        }

        return mealsSet;
    }

    public Set<Day> getDaysForMealId(int mealId) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();

        TypedQuery<Meal> query = entityManager.createQuery(DAYS_FOR_MEALID_JPQL, Meal.class);
        query.setParameter("id", mealId);
        Meal meal = query.getSingleResult();
        Set<Day> days = meal.getDays();

        entityManager.getTransaction().commit();
        entityManager.close();

        return days;
    }

    public Set<Period> getPeriodsForMealId(int mealId) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();

        TypedQuery<Meal> query = entityManager.createQuery(PERIODS_FOR_MEALID_JPQL, Meal.class);
        query.setParameter("id", mealId);
        Meal meal = query.getSingleResult();
        Set<Period> periods = meal.getPeriods();

        entityManager.getTransaction().commit();
        entityManager.close();

        return periods;
    }

    public boolean addPeriodForMeal(int mealId, int periodId) {
        boolean status = false;
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            Meal targetMeal = entityManager.find(Meal.class, mealId);
            Period newPeriod = entityManager.find(Period.class, periodId);

            boolean add = targetMeal.addPeriod(newPeriod);

            entityManager.persist(targetMeal);

            entityManager.getTransaction().commit();
            entityManager.close();

            status = add;

        } catch (PersistenceException e) {
            System.out.println("There was an error. Adding failed!");
            entityManager.close();
        }

        return status;
    }

    public boolean addDayForMeal(int mealId, int dayId) {
        boolean status = false;
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            Meal targetMeal = entityManager.find(Meal.class, mealId);
            Day newDay = entityManager.find(Day.class, dayId);

            boolean add = targetMeal.addDay(newDay);

            entityManager.persist(targetMeal);

            entityManager.getTransaction().commit();
            entityManager.close();

            status = add;

        } catch (PersistenceException e) {
            System.out.println("There was an error. Adding failed!");
            entityManager.close();
        }

        return status;
    }

    public boolean updateMealName(int mealId, String newName) {
        boolean status = false;
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();
            Meal targetMeal = entityManager.find(Meal.class, mealId);

            targetMeal.setName(newName);
            entityManager.persist(targetMeal);

            entityManager.getTransaction().commit();
            entityManager.close();
            status = true;

        } catch (PersistenceException e) {
            entityManager.close();
        }

        return status;
    }

    public boolean replaceDayForMeal(int mealId, int currentDayId, int newDayId) {
        boolean status = false;
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            Meal targetMeal = entityManager.find(Meal.class, mealId);
            Day currentDay = entityManager.find(Day.class, currentDayId);
            Day newDay = entityManager.find(Day.class, newDayId);

            boolean remove = targetMeal.removeDay(currentDay);
            boolean add = targetMeal.addDay(newDay);

            entityManager.persist(targetMeal);

            entityManager.getTransaction().commit();
            entityManager.close();

            status = remove && add;

        } catch (PersistenceException e) {
            System.out.println("There was an error. Operation failed!");
            entityManager.close();
        }

        return status;
    }

    public boolean replacePeriodForMeal(int mealId, int currentHourId, int newHourId) {
        boolean status = false;
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            Meal targetMeal = entityManager.find(Meal.class, mealId);
            Period currentPeriod = entityManager.find(Period.class, currentHourId);
            Period newPeriod = entityManager.find(Period.class, newHourId);

            boolean remove = targetMeal.removePeriod(currentPeriod);
            boolean add = targetMeal.addPeriod(newPeriod);

            entityManager.persist(targetMeal);

            entityManager.getTransaction().commit();
            entityManager.close();

            status = remove && add;

        } catch (PersistenceException e) {
            System.out.println("There was an error. Operation failed!");
            entityManager.close();
        }

        return status;
    }

    public boolean removePeriodForMeal(int mealId, int periodId) {
        boolean status = false;
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            Meal targetMeal = entityManager.find(Meal.class, mealId);
            Period currentPeriod = entityManager.find(Period.class, periodId);

            boolean remove = targetMeal.removePeriod(currentPeriod);

            entityManager.persist(targetMeal);

            entityManager.getTransaction().commit();
            entityManager.close();

            status = remove;

        } catch (PersistenceException e) {
            System.out.println("There was an error. Operation failed!");
            entityManager.close();
        }

        return status;
    }

    public boolean removeDayForMeal(int mealId, int dayId) {
        boolean status = false;
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            Meal targetMeal = entityManager.find(Meal.class, mealId);
            Day currentDay = entityManager.find(Day.class, dayId);

            boolean remove = targetMeal.removeDay(currentDay);

            entityManager.persist(targetMeal);

            entityManager.getTransaction().commit();
            entityManager.close();

            status = remove;

        } catch (PersistenceException e) {
            System.out.println("There was an error. Operation failed!");
            entityManager.close();
        }

        return status;
    }

    public boolean removeMeal(int mealId) {
        boolean status = false;
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();
            Meal targetMeal = entityManager.find(Meal.class, mealId);

            entityManager.remove(targetMeal);

            entityManager.getTransaction().commit();
            entityManager.close();

            status = true;

        } catch (PersistenceException e) {
            entityManager.close();
        }

        return status;
    }
}