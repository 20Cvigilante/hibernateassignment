package net.therap.canteen.dao;

import net.therap.canteen.entity.Day;
import net.therap.canteen.entity.Period;
import net.therap.canteen.entity.Meal;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import java.util.HashSet;
import java.util.Set;

import static net.therap.canteen.dao.CanteenDao.getEntityManager;

/**
 * @author musa.khan
 * @since 06/12/2020
 */
public class DayDao {

    private static final String MEALS_FOR_DAYID_JPQL = "SELECT d " +
                                                        "FROM Day d " +
                                                        "LEFT JOIN FETCH d.meals " +
                                                        "WHERE d.id = :id";
    private static final String PERIODS_FOR_DAYID_JPQL = "SELECT d " +
                                                        "FROM Day d " +
                                                        "LEFT JOIN FETCH d.periods " +
                                                        "WHERE d.id = :id";

    public Set<Meal> getMealsForDayId(int dayId) {
        Set<Meal> meals = new HashSet<>();
        EntityManager entityManager = null;

        try {

            entityManager = getEntityManager();
            entityManager.getTransaction().begin();

            TypedQuery<Day> query = entityManager.createQuery(MEALS_FOR_DAYID_JPQL, Day.class);
            query.setParameter("id", dayId);
            Day day = query.getSingleResult();
            meals = day.getMeals();

            entityManager.getTransaction().commit();
            entityManager.close();

        } catch(PersistenceException e) {
            entityManager.close();
        }

        return meals;
    }

    public Set<Period> getPeriodsForDayId(int dayId) {
        Set<Period> periods = new HashSet<>();
        EntityManager entityManager = null;

        try {

            entityManager = getEntityManager();
            entityManager.getTransaction().begin();

            TypedQuery<Day> query = entityManager.createQuery(PERIODS_FOR_DAYID_JPQL, Day.class);
            query.setParameter("id", dayId);
            Day day = query.getSingleResult();
            periods = day.getHours();

            entityManager.getTransaction().commit();
            entityManager.close();

        } catch(PersistenceException e) {
            entityManager.close();
        }

        return periods;
    }
}