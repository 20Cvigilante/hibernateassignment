package net.therap.canteen.dao;

import net.therap.canteen.entity.Day;
import net.therap.canteen.entity.Period;
import net.therap.canteen.entity.Meal;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static net.therap.canteen.dao.CanteenDao.getEntityManager;
import static net.therap.canteen.util.CanteenUtility.getDayFromEnum;

/**
 * @author musa.khan
 * @since 06/12/2020
 */
public class PeriodDao {

    private static final String GET_DAY_BY_NAME_JPQL = "SELECT d " +
                                                        "FROM Day d " +
                                                        "WHERE d.name = :name";
    private static final String GET_PERIOD_BY_NAME_JPQL = "SELECT p " +
                                                        "FROM Period p " +
                                                        "WHERE p.name = :name";
    private static final String ALL_PERIODS_QUERY_JPQL = "SELECT periods " +
                                                        "FROM Period periods";
    private static final String DAYS_FOR_PERIODID_JPQL = "SELECT p " +
                                                        "FROM Period p " +
                                                        "LEFT JOIN FETCH p.days " +
                                                        "WHERE p.id = :id";
    private static final String MEALS_FOR_PERIODID_JPQL = "SELECT p " +
                                                        "FROM Period p " +
                                                        "LEFT JOIN FETCH p.meals " +
                                                        "WHERE p.id = :id";

    public boolean insertPeriod(String periodName, List<Integer> daysSelected) {
        boolean status = false;

        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            Period period = new Period(periodName);

            for (Integer daySelected : daysSelected) {

                String dayName = getDayFromEnum(daySelected);

                TypedQuery<Day> queryDay = entityManager.createQuery(GET_DAY_BY_NAME_JPQL, Day.class);
                queryDay.setParameter("name", dayName);

                Day targetDay = queryDay.getSingleResult();
                period.addDay(targetDay);
            }

            entityManager.persist(period);
            entityManager.getTransaction().commit();
            entityManager.close();
            status = true;

        } catch (PersistenceException e) {

            entityManager.close();

            if (e.getMessage().contains("duplicate")) {
                System.out.println("Duplicate problems!");
            }
            else {
                System.out.println("There was an error. Operation failed!");
                e.printStackTrace();
            }
        }
        return status;
    }

    public Period getPeriodByName(String periodName) {
        Period period = null;

        EntityManager entityManager = getEntityManager();

        try {
            TypedQuery<Period> query = entityManager.createQuery(GET_PERIOD_BY_NAME_JPQL, Period.class);
            query.setParameter("name", periodName);
            period = query.getSingleResult();

            entityManager.close();

        } catch (PersistenceException | IllegalArgumentException e) {

            entityManager.close();
        }

        return period;
    }

    public Period getPeriodById(int periodId) {
        EntityManager entityManager = getEntityManager();
        Period period = null;

        try {
            period = entityManager.find(Period.class, periodId);
            entityManager.close();

        } catch (PersistenceException e) {
            entityManager.close();
        }

        return period;
    }

    public static Set<Period> getAllPeriods() {
        EntityManager entityManager = getEntityManager();
        Set<Period> periodsSet = null;

        try {
            TypedQuery<Period> allHoursQuery = entityManager.createQuery(ALL_PERIODS_QUERY_JPQL, Period.class);
            periodsSet = new HashSet<>(allHoursQuery.getResultList());
            entityManager.close();

        } catch (PersistenceException e) {
            entityManager.close();
        }

        return periodsSet;
    }

    public Set<Day> getDaysForPeriodId(int periodId) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();

        TypedQuery<Period> query = entityManager.createQuery(DAYS_FOR_PERIODID_JPQL, Period.class);
        query.setParameter("id", periodId);
        Period period = query.getSingleResult();
        Set<Day> days = period.getDays();

        entityManager.getTransaction().commit();
        entityManager.close();

        return days;
    }

    public Set<Meal> getMealsForPeriodId(int periodId) {
        EntityManager entityManager = getEntityManager();
        entityManager.getTransaction().begin();

        TypedQuery<Period> query = entityManager.createQuery(MEALS_FOR_PERIODID_JPQL, Period.class);
        query.setParameter("id", periodId);

        Period period = query.getSingleResult();
        Set<Meal> meals = period.getMeals();

        entityManager.getTransaction().commit();
        entityManager.close();

        return meals;
    }

    public boolean addMealForPeriod(int periodId, int mealId) {
        boolean status = false;
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            Period targetPeriod = entityManager.find(Period.class, periodId);
            Meal newMeal = entityManager.find(Meal.class, mealId);

            boolean add = targetPeriod.addMeal(newMeal);

            entityManager.persist(targetPeriod);

            entityManager.getTransaction().commit();
            entityManager.close();

            status = add;

        } catch (PersistenceException e) {

            System.out.println("There was an error. Operation failed!");
            entityManager.close();
        }

        return status;
    }

    public boolean addDayForPeriod(int periodId, int dayId) {
        boolean status = false;
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            Period targetPeriod = entityManager.find(Period.class, periodId);
            Day newDay = entityManager.find(Day.class, dayId);

            boolean add = targetPeriod.addDay(newDay);

            entityManager.persist(targetPeriod);

            entityManager.getTransaction().commit();
            entityManager.close();

            status = add;

        } catch (PersistenceException e) {

            System.out.println("There was an error. Operation failed!");
            entityManager.close();
        }

        return status;
    }

    public boolean updatePeriodName(int periodId, String newName) {
        boolean status = false;
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            Period targetPeriod = entityManager.find(Period.class, periodId);
            targetPeriod.setName(newName);

            entityManager.persist(targetPeriod);

            entityManager.getTransaction().commit();
            entityManager.close();

            status = true;

        } catch (PersistenceException e) {

            System.out.println("There was an error. Operation failed!");
            entityManager.close();
        }

        return status;
    }

    public boolean replaceDayForPeriod(int periodId, int currentDayId, int newDayId) {
        boolean status = false;
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            Period targetPeriod = entityManager.find(Period.class, periodId);
            Day currentDay = entityManager.find(Day.class, currentDayId);
            Day newDay = entityManager.find(Day.class, newDayId);

            boolean remove = targetPeriod.removeDay(currentDay);
            boolean add = targetPeriod.addDay(newDay);

            entityManager.persist(targetPeriod);

            entityManager.getTransaction().commit();
            entityManager.close();

            status = remove && add;

        } catch (PersistenceException e) {

            System.out.println("There was an error. Operation failed!");
            entityManager.close();
        }

        return status;
    }

    public boolean replaceMealForPeriod(int periodId, int currentMealId, int newMealId) {
        boolean status = false;
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            Period targetPeriod = entityManager.find(Period.class, periodId);
            Meal currentMeal = entityManager.find(Meal.class, currentMealId);
            Meal newMeal = entityManager.find(Meal.class, newMealId);

            boolean remove = targetPeriod.removeMeal(currentMeal);
            boolean add = targetPeriod.addMeal(newMeal);

            entityManager.persist(targetPeriod);

            entityManager.getTransaction().commit();
            entityManager.close();

            status = remove && add;

        } catch (PersistenceException e) {

            System.out.println("There was an error. Operation failed!");
            entityManager.close();
        }

        return status;
    }

    public boolean removeMealForPeriod(int periodId, int mealId) {
        boolean status = false;
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            Period targetPeriod = entityManager.find(Period.class, periodId);
            Meal currentMeal = entityManager.find(Meal.class, mealId);

            boolean remove = targetPeriod.removeMeal(currentMeal);

            entityManager.persist(targetPeriod);

            entityManager.getTransaction().commit();
            entityManager.close();

            status = remove;

        } catch (PersistenceException e) {

            System.out.println("There was an error. Operation failed!");
            entityManager.close();
        }

        return status;
    }

    public boolean removeDayForPeriod(int periodId, int dayId) {
        boolean status = false;
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            Period targetPeriod = entityManager.find(Period.class, periodId);
            Day currentDay = entityManager.find(Day.class, dayId);

            boolean remove = targetPeriod.removeDay(currentDay);

            entityManager.persist(targetPeriod);

            entityManager.getTransaction().commit();
            entityManager.close();

            status = remove;

        } catch (PersistenceException e) {

            System.out.println("There was an error. Operation failed!");
            entityManager.close();
        }

        return status;
    }

    public boolean removePeriod(int periodId) {
        boolean status = false;
        EntityManager entityManager = getEntityManager();

        try {
            entityManager.getTransaction().begin();

            Period targetPeriod = entityManager.find(Period.class, periodId);
            entityManager.remove(targetPeriod);

            entityManager.getTransaction().commit();
            entityManager.close();

            status = true;

        } catch (PersistenceException e) {

            System.out.println("There was an error. Operation failed!");
            entityManager.close();
        }

        return status;
    }
}