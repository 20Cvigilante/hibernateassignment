package net.therap.canteen.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author musa.khan
 * @since 27/11/2020
 */
public class CanteenDao {

    private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("canteen_persistence_unit");

    public static EntityManager getEntityManager() {

        return emf.createEntityManager();
    }
}