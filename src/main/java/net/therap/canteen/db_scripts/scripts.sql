CREATE TABLE IF NOT EXISTS days(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(20) NOT NULL unique
);

CREATE TABLE IF NOT EXISTS periods(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(100) NOT NULL unique
);

CREATE TABLE IF NOT EXISTS meals(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(100) NOT NULL unique
);

CREATE TABLE IF NOT EXISTS days_periods(
    day_id Integer references days(id),
    period_id Integer references periods(id),
    Unique(day_id, period_id)
);

CREATE TABLE IF NOT EXISTS periods_meals(
    period_id Integer references periods(id),
    meal_id Integer references meals(id),
    Unique(period_id, meal_id)
);

CREATE TABLE IF NOT EXISTS days_meals(
    day_id Integer references days(id),
    meal_id Integer references meals(id),
    Unique(day_id, meal_id)
);